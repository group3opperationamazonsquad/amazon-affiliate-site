﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Amazon_Affiliate.Data;

namespace Amazon_Affiliate.Pages.Items
{
    public class IndexModel : PageModel
    {
        private readonly Amazon_Affiliate.Data.ApplicationDbContext _context;

        public IndexModel(Amazon_Affiliate.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Item> Item { get;set; }

        public async Task OnGetAsync()
        {
            Item = await _context.Items.ToListAsync();
        }
    }
}
