﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Amazon_Affiliate.Data
{
    public class Item
    {
        public int ID { get; set; }
        public string ItemName { get; set; }
        public AppSiteData Creator { get; set; }
        public string Description { get; set; }
        public ItemRating Rating { get; set; }
        public string AffiliateLink { get; set; }
        public ItemCategory Category { get; set; }
        public DateTime DateAdded { get; set; }
        public byte[] Picture { get; set; }

        public enum ItemCategory
        {
            Clothing_and_Shoes = 0,
            Jewelery = 1,
            Books = 2,
            Movies_Music_and_Games = 3,
            Office = 4,
            Home_Garden_and_Tools = 4,
            Pet_Supplies = 5,
            Food_and_Grocery = 6,
            Beauty_and_Health = 7,
            Toys_Kids_and_Baby = 8,
            Sports_and_Outdoors = 9,
            Automotive_and_Industrial = 10,
        }

        public enum ItemRating
        {
            Terrible = 0,
            Bad = 1,
            Average = 2,
            Good = 3,
            Great = 4,
            Excellent = 5,
        }

    }

    public class PictureUpload : PageModel
    {
        [BindProperty]
        public PictureUploadDb PictureU { get; set; }
    }

    public class PictureUploadDb
    {
        [Required]
        [Display(Name = "File")]
        public IFormFile FormFile { get; set; }
    }
}
