﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amazon_Affiliate.Data
{
    public class AppSiteData
    {
        public int Id { get; set; }
        public string OwnerFName { get; set; }
        public string OwnerLName { get; set; }
        public string SiteName { get; set; }

    }
}
